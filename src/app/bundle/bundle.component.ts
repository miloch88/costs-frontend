import {Component, OnInit} from '@angular/core';
import {HttpService} from '../services/http.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../login/auth/token-storage.service';

@Component({
  selector: 'app-bundle',
  templateUrl: './bundle.component.html',
  styleUrls: ['./bundle.component.css']
})
export class BundleComponent implements OnInit {

  info: any;
  allBundles$: any;

  sort = [
    {name: 'Name', value: 1},
    {name: 'Date buy', value: 2},
    {name: 'Cost USD', value: 3},
    {name: 'Cost PLN', value: 4},
    {name: 'Sold', value: 5}
  ];
  defaultValue = '';

  constructor(
    private httpService: HttpService,
    private router: Router,
    private token: TokenStorageService) {
  }

  ngOnInit(): void {

    this.info = {
      username: this.token.getUsername(),
    };

    this.httpService.getAllBundlesByUser(this.info.username)
      .subscribe(data => {
          if (data != null) {
            this.allBundles$ = data;
          }
        }
      );
  }

  remove(bundle: any) {
    this.httpService.deleteBundle(bundle);
    this.router.navigate(['/bundle']);
    window.location.reload();
  }

  sum(bundle: any) {
    let sum = 0;
    for (let i = 0; i < bundle.gameList.length; i++) {
      sum += (bundle.gameList[i].salePrcie);
    }
    return sum.toPrecision(4);
  }

  modify(bundle: any) {
    this.router.navigate(['/bundle/modify', bundle.id]);
  }

  sumPaid(bundle: any) {
    let sum = 0;

    for (let i = 0; i < bundle.length; i++) {
      sum += (bundle[i].costPLN);
    }
    return sum.toPrecision(5);
  }

  sumSold(bundle: any) {
    let sum = 0;

    for (let i = 0; i < bundle.length; i++) {
      sum += parseFloat(this.sum(bundle[i]));
    }

    return sum.toPrecision(5);
  }

  setSort(sortId: string) {
    this.httpService.getSortByDate(sortId)
      .subscribe(data => {
        if (data != null) {
          this.allBundles$ = data;
          console.log(this.allBundles$);
        }
      });
  }

  setSortAndUser(sortId: string) {

      this.httpService.getSortAndUser(sortId, this.info.username)
        .subscribe(data => {
          if (data != null) {
            this.allBundles$ = data;
          }
        });
  }
}
