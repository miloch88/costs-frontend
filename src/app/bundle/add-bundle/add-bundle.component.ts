import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {HttpService} from '../../services/http.service';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../login/auth/token-storage.service';

@Component({
  selector: 'app-add-bundle',
  templateUrl: './add-bundle.component.html',
  styleUrls: ['./add-bundle.component.css']
})
export class AddBundleComponent implements OnInit {

  info: any;
  bundleForm: FormGroup;

  constructor(
    private httpService: HttpService,
    private router: Router,
    private token: TokenStorageService) {
  }

  ngOnInit() {
    this.bundleForm = new FormGroup({
      name: new FormControl(null),
      dateBuy: new FormControl(null),
      costUSD: new FormControl(null),
      costPLN: new FormControl(null)
    });
    this.info = {
      username: this.token.getUsername(),
    };
  }


  onSubmit() {
    const name = this.bundleForm.value.name;
    const dateBuy = this.bundleForm.value.dateBuy;
    const costUSD = this.bundleForm.value.costUSD;
    const costPLN = this.bundleForm.value.costPLN;
    const user = this.info;

    console.log(name, dateBuy, costUSD, costPLN, user);
    this.httpService.addNewBundleToUser(name, dateBuy, costUSD, costPLN, user);
    window.location.reload();
    this.router.navigate(['/bundle']);


    // this.httpService.postNewBundle(name, dateBuy, costUSD, costPLN);
    // window.location.reload();
    // this.router.navigate(['/bundle']);
  }
}
