import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyBundleComponent } from './modify-bundle.component';

describe('ModifyBundleComponent', () => {
  let component: ModifyBundleComponent;
  let fixture: ComponentFixture<ModifyBundleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifyBundleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyBundleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
