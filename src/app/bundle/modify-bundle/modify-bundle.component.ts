import {Component, OnInit} from '@angular/core';
import {HttpService} from '../../services/http.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-modify-bundle',
  templateUrl: './modify-bundle.component.html',
  styleUrls: ['./modify-bundle.component.css']
})
export class ModifyBundleComponent implements OnInit {

  id: any;
  bundle$: any;
  modifyForm: FormGroup;

  constructor(
    private httpService: HttpService,
    private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
    });

    this.httpService.getBundle(this.id)
      .subscribe(data => {
        if (data != null) {
          this.bundle$ = data;
          console.log(this.bundle$);
          this.getValue();
        }
      });

    this.modifyForm = new FormGroup({
      name: new FormControl(),
      dateBuy: new FormControl(),
      costUSD: new FormControl(),
      costPLN: new FormControl(),
      gameList: new FormControl()
    });
  }

  onSubmit() {
    let name = this.modifyForm.value.name;
    if (name === '') {
      name = null;
    }

    let dateBuy = this.modifyForm.value.dateBuy;
    if (dateBuy === '') {
      dateBuy = null;
    }


    const costUSD = this.modifyForm.value.costUSD;
    const costPLN = this.modifyForm.value.costPLN;

    // console.log(dateBuy);
    this.httpService.modifyBundle(this.id, name, dateBuy, costUSD, costPLN);
    this.router.navigate(['/bundle']);
    window.location.reload();
  }

  getValue(): void {
    this.modifyForm.controls['name'].setValue(this.bundle$.name);
    this.modifyForm.controls['dateBuy'].setValue(this.bundle$.dateBuy);
    this.modifyForm.controls['costUSD'].setValue(this.bundle$.costUSD);
    this.modifyForm.controls['costPLN'].setValue(this.bundle$.costPLN);
  }

  addNewGame(id) {
    this.router.navigate(['/bundle/game/add', id]);
  }

  removeGame(id: any) {
    this.httpService.deleteGame(id);
    this.router.navigate(['/bundle/modify', this.id]);
    window.location.reload();
  }

  modifyGame(id: any) {
    this.router.navigate(['/bundle/games', id]);
    // window.location.reload();

  }
}
