import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

@Injectable()
export class HttpService {

  private hostUrl = 'http://localhost:8080/bundle';

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(private http: HttpClient) {
  }

  getAllBundles() {
    return this.http.get<any>(this.hostUrl);
  }

  getAllBundlesByUser(username: string) {
    const body = JSON.stringify({username: username});
    return this.http.post<any>(this.hostUrl + '/byuser', body, this.httpOptions);
  }

  postNewBundle(name: string, dateBuy: string, costUSD: number, costPLN: number) {
    const body = JSON.stringify({costPLN: costPLN, costUSD: costUSD, dateBuy: dateBuy, name: name});
    console.log(body);
    return this.http.post<any>(this.hostUrl, body, this.httpOptions).subscribe();
  }

  deleteBundle(bundle: any) {
    return this.http.delete<any>(this.hostUrl + '/' + bundle.id).subscribe();
  }

  getBundle(id: any) {
    return this.http.get<any>(this.hostUrl + '/' + id);
  }

  modifyBundle(id: number, name: string, dateBuy: any, costUSD: number, costPLN: number) {
    const body = JSON.stringify({costPLN: costPLN, costUSD: costUSD, dateBuy: dateBuy, name: name});
    return this.http.put<any>(this.hostUrl + '/' + id, body, this.httpOptions).subscribe();
  }

  getBundleGames(id: any) {
    return this.http.get<any>(this.hostUrl + '/' + id + '/games');
  }

  postNewGame(id: number, name: string, price: number) {
    const body = JSON.stringify({id: id, name: name, salePrcie: price});
    return this.http.post<any>(this.hostUrl + '/games', body, this.httpOptions).subscribe();
  }

  deleteGame(id: any) {
    return this.http.delete<any>(this.hostUrl + '/games/' + id).subscribe();
  }

  getGame(id: any) {
    return this.http.get<any>(this.hostUrl + '/games/' + id);
  }

  modifyGame(id: number, name: string, price: number) {
    const body = JSON.stringify({name: name, salePrcie: price});
    return this.http.put<any>(this.hostUrl + '/games/' + id, body, this.httpOptions).subscribe();

  }

  getBundleId(id: any) {
    return this.http.get<any>(this.hostUrl + '/getBundleId/' + id);
  }

  getSortByDate(sortId: any) {
    const param = new HttpParams().set('sortId', sortId + '');
    return this.http.get<any>(this.hostUrl + '/sort', {params: param});
  }

  addNewBundleToUser(name: any, dateBuy: any, costUSD: any, costPLN: any, user: any) {
    const body = JSON.stringify({costPLN: costPLN, costUSD: costUSD, dateBuy: dateBuy, name: name});
    // const param = new HttpParams().set('user', user.username);
    return this.http.post<any>(this.hostUrl + '/add?user=' + user.username + '', body, this.httpOptions).subscribe();
  }

  getSortAndUser(sortId: any, username: string) {
    const body = JSON.stringify({sortId: sortId, username: username});
    return this.http.post<any>(this.hostUrl + '/sortanduser', body, this.httpOptions);
  }
}
