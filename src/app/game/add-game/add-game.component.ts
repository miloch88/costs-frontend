import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup} from '@angular/forms';
import {HttpService} from '../../services/http.service';

@Component({
  selector: 'app-add-game',
  templateUrl: './add-game.component.html',
  styleUrls: ['./add-game.component.css']
})
export class AddGameComponent implements OnInit {

  id: any;
  gameForm: FormGroup;

  constructor(
    private httpService: HttpService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {

    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
    });

    this.gameForm = new FormGroup({
      name: new FormControl(null),
      price: new FormControl (null)
    });
  }

  onSubmit() {
    const name = this.gameForm.value.name;
    const price = this.gameForm.value.price;

    this.httpService.postNewGame(this.id, name, price);
    window.location.reload();
    this.router.navigate(['/bundle/modify', this.id]);
  }
}
