import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {HttpService} from '../../services/http.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-modify-game',
  templateUrl: './modify-game.component.html',
  styleUrls: ['./modify-game.component.css']
})
export class ModifyGameComponent implements OnInit {

  id: any;
  bundleID: any;
  game$: any;
  gameForm: FormGroup;

  constructor(
    private httpService: HttpService,
    private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
    });

    this.httpService.getBundleId(this.id)
      .subscribe(data => {
        if (data != null) {
          this.bundleID = data;
        }
      });

    this.httpService.getGame(this.id)
      .subscribe(data => {
        if (data != null) {
          this.game$ = data;
          this.getValue();
        }
      });

    this.gameForm = new FormGroup({
      name: new FormControl(null),
      price: new FormControl (null)
    });
  }

  onSubmit() {
    let name = this.gameForm.value.name;
    if (name === '') {
      name = null;
    }

    const price = this.gameForm.value.price;


    this.httpService.modifyGame(this.id, name, price);
    this.router.navigate(['/bundle/modify/', this.bundleID]);
    // window.location.reload();
  }

  getValue(): void {
    this.gameForm.controls['name'].setValue(this.game$.name);
    this.gameForm.controls['price'].setValue(this.game$.salePrcie);
  }
}
