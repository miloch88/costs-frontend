import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {HttpService} from './services/http.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BundleComponent } from './bundle/bundle.component';
import { AddBundleComponent } from './bundle/add-bundle/add-bundle.component';
import {AppRoutingModule} from './app.routing.module';
import {ModifyBundleComponent} from './bundle/modify-bundle/modify-bundle.component';
import {AddGameComponent} from './game/add-game/add-game.component';
import {ModifyGameComponent} from './game/modify-game/modify-game.component';
import {RegisterComponent} from './login/register/register.component';
import {httpInterceptorProviders} from './login/auth/AuthInterceptor';
import {LoginComponent} from './login/login/login.component';
import {ProfileComponent} from './login/profile/profile.component';

@NgModule({
  declarations: [
    AppComponent,
    BundleComponent,
    AddBundleComponent,
    ModifyBundleComponent,
    AddGameComponent,
    ModifyGameComponent,
    RegisterComponent,
    LoginComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [HttpService, httpInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
