import {Component, OnInit} from '@angular/core';
import {SignUpInfo} from '../auth/SignUpInfo';
import {AuthService} from '../auth/auth.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  form: any = {};
  signupInfo: SignUpInfo;
  isSignedUp = false;
  isSignUpFailed = false;
  errorMessage = '';

  userForm: FormGroup;

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
    this.userForm = new FormGroup({
      name: new FormControl(null, [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(50)]),
      username: new FormControl(null, [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(50)]),
      email: new FormControl(null, [
        Validators.required,
        Validators.email,
        Validators.maxLength(50)]),
      password: new FormControl(null,[
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(100)])
    });
  }

  onSubmit() {
    // console.log(this.form);
    //
    // this.signupInfo = new SignUpInfo(
    //   this.form.name,
    //   this.form.username,
    //   this.form.email,
    //   this.form.password);

    console.log(this.userForm);

    this.signupInfo = new SignUpInfo(
      this.userForm.value.name,
      this.userForm.value.username,
      this.userForm.value.email,
      this.userForm.value.password);

    this.authService.signUp(this.signupInfo).subscribe(
      data => {
        console.log(data);
        this.isSignedUp = true;
        this.isSignUpFailed = false;
      },
      error => {
        console.log(error);
        this.errorMessage = error;
        this.isSignUpFailed = true;
      }
    );
  }
}
