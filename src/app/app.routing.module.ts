import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BundleComponent} from './bundle/bundle.component';
import {AddBundleComponent} from './bundle/add-bundle/add-bundle.component';
import {ModifyBundleComponent} from './bundle/modify-bundle/modify-bundle.component';
import {AddGameComponent} from './game/add-game/add-game.component';
import {ModifyGameComponent} from './game/modify-game/modify-game.component';
import {RegisterComponent} from './login/register/register.component';
import {LoginComponent} from './login/login/login.component';
import {ProfileComponent} from './login/profile/profile.component';

const appRoutes: Routes = [
  {
    path: 'bundle',
    component: BundleComponent
  },
  {
    path: 'bundle/add',
    component: AddBundleComponent
  },
  {
    path: 'bundle/modify/:id',
    component: ModifyBundleComponent
  },
  {
    path: 'bundle/game/add/:id',
    component: AddGameComponent
  },
  {
    path: 'bundle/games/:id',
    component: ModifyGameComponent
  },
  {
    path: 'signup',
    component: RegisterComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'profile',
    component: ProfileComponent
  }
];


@NgModule({
  imports: [RouterModule.forRoot(appRoutes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
